<?php

namespace www;

use src\Calculator;

require_once __DIR__ . '/../bootstrap.php';

$calculator = new Calculator();

if (array_key_exists('a', $_GET) && array_key_exists('b', $_GET)) {
    $result = $calculator->sum(
        $_GET['a'],
        $_GET['b']
    );
    echo $result;
}

echo 'NaN';


