<?php

namespace src;

require_once __DIR__ . '/../bootstrap.php';

class Calculator
{

    /**
     * @param int $a
     * @param int $b
     * @return int
     */
    public function sum($a, $b)
    {
        return $a + $b;
    }

    /**
     * @param int $num
     * @param int $den
     * @return int|string
     */
    public function div($num, $den)
    {
      return $den != 0 ? $num/$den : "This division is undefined";
    }
}